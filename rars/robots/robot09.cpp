/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <cmath>

//--------------------------------------------------------------------------
//                           Class Robot09
//--------------------------------------------------------------------------


class Robot09 : public Driver
{
public:
    // Konstruktor
    Robot09()
    {
        // Der Name des Robots
        m_sName = "Robot09";
        // Namen der Autoren
        m_sAuthor = "Malte Neumann";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oGREEN;
        m_iTailColor = oWHITE;
        m_sBitmapName2D = "car_gray_green";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }



//--------------------------------------------------------------------------
//                          F U N K T I O N E N
//--------------------------------------------------------------------------




//--------------------------------------------------------------------------
//Funktion berechnet die maximal mögliche Kurvengeschwindigkeit in Abh. vom Kurvenradius
//
//  radius (in)
//
//  max_crn_spd (return)
//--------------------------------------------------------------------------
    double max_crn_spd (double radius)
    {
        const double CENPET_FRC = 32.2;     //Erdbeschleunigung
        const double COEF_FRCTN = 0.95;      //Reibungskoeffizient
        return sqrt(fabs(radius*COEF_FRCTN*CENPET_FRC));
    }

//--------------------------------------------------------------------------
//Funktion berechnet den kritischen Abstand (Bremsweg) um von Geschwindigkeit 1 (v1) auf eine andere Geschwindigkeit (v2) zu Bremsen
//
//  v1 (in)
//  v2 (in)
//
//  CritDist (return)
//--------------------------------------------------------------------------
    double CritDist(double v1, double v2)
    {
        double delta;
        const double BREAK_ACC = -33;     //Bremsbeschleunigung
        delta = v2 - v1;
        if(delta > 0.0)
            return(0.0);
        return (v1 + 0.5 * delta) * delta / BREAK_ACC;
    }

//--------------------------------------------------------------------------
//Funktion berechnet den Lenkwinkel in Abhaengigkeit von der aktuellen Situation
//
//  situation s (in)
//
//  alpha (return)
//--------------------------------------------------------------------------
    double steering (situation& s)
    {

        const double DAMP = (s.vn/s.v)*1.1;         //Dämpfungsterm für die Lenkwinkelsteuerung
        const double STEER_GAIN = 0.5;              //Faktor zur Schwächung der Lenkung
        const double width = s.to_lft+s.to_rgt;     //Berechnung der Streckenbreite

        double alpha = 0.0;


        if (s.cur_rad == 0)     //wir sind auf einer Geraden

            alpha=  (STEER_GAIN*(s.to_lft - s.to_rgt) / (width) - DAMP);

        else if (s.cur_rad < 0)     //wir sind in einer Rechtskurve

            alpha = (STEER_GAIN*((s.to_lft - width + CARWID) / (width)) - DAMP);

        else if (s.cur_rad > 0)     //wir sind in einer Linkskurve

            alpha =   (STEER_GAIN*((s.to_lft -CARWID) / (width)) - DAMP);

        //Ausgabe
        return alpha;
    }
//--------------------------------------------------------------------------
//Funktion berechnet die optimale Geschwindigkeit in Abhaengigkeit von der aktuellen Situation
//
//  situation s (in)
//
//  speed (return)
//--------------------------------------------------------------------------
    double velocity (situation &s)
    {
        double speed = 0.0;
        const double width = s.to_lft+s.to_rgt;     //Berechnung der Streckenbreite

        //wir sind auf einer Geraden und weit genug von nächstere Kurve entfernt-> Vollgas
        if (s.cur_rad == 0 && s.to_end > CritDist(s.v, max_crn_spd(s.nex_rad+CARWID)))
            speed = s.v+100;

        //wir sind auf einer Geraden und die nächste Kurve ist näher als der Krit DIST
        else if (s.cur_rad == 0 && s.nex_rad != 0 && s.to_end < CritDist(s.v, max_crn_spd(s.nex_rad+CARWID)))
            speed = max_crn_spd(s.nex_rad+CARWID+width/2);

        //wir sind in einer Kurve und das nächste Segment ist eine Gerade es wurde noch nicht die Hälfte der Kurve gefahren -> maximal mögliche Kurvengeschwindigkeit fahren
        else if (s.cur_rad != 0 && s.nex_rad == 0 && s.to_end > s.cur_len/2)
            speed = max_crn_spd(s.cur_rad+CARWID+width/2);

        //wir sind in einer Kurve und das nächste Segment ist eine Gerade und die Kurve ist schon über die Hälfte befahren -> schneller werden
        else if (s.cur_rad != 0 && s.nex_rad == 0 && s.to_end < s.cur_len/2)
            speed = max_crn_spd(s.cur_rad+CARWID+width/2)*5;

        //wir sind in einer Kurve und die nächste Kurve ist enger als die aktuelle und es ist nur noch 1/3 des aktuellen Segments zu befahren -> auf nächste Kurvengeschwindigkeit runterbremsen
        else if(s.cur_rad != 0 && s.cur_rad > s.nex_rad && s.to_end < s.cur_len/3)
            speed = max_crn_spd(CARWID + s.nex_rad + width/2);

        //wir sind in einer Kurve -> maximal mögliche Geschwindigkeit in Abh vom Radius fahren
        else
            speed = max_crn_spd((s.cur_rad+CARWID+width/2));

        //Ausgabe
        return speed;
    }

//--------------------------------------------------------------------------
//                          D R I V E  V E C T O R
//--------------------------------------------------------------------------

    con_vec drive(situation& s)
    {

        con_vec result = CON_VEC_EMPTY;     //Ergebnisvektor


        //Benzinmenge vor dem Start definieren
        if(s.starting == 1)
            result.fuel_amount = 50;


        //Im Falle eines Unfalls wird der Robot auf die Strecke gefuehrt
        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))        //wir sind von der Strecke abgekommen
            return result;


        //Boxenstopp-Regelung
        if(s.damage > 15000 || s.fuel < 5)
        {
            result.request_pit = 1;
            result.repair_amount = s.damage*0.65;             //es sollen 75% repariert werden
            result.fuel_amount = 50- s.fuel;                  //Es soll immer auf 50 Gallonen aufgefüllt werden
        }


        //Beschreiben des Ausgabevektors
        result.alpha = steering(s);
        result.vc = velocity(s);


        //Ausgabe des Ergebnisvektors
        return result;
    }

};


/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot09Instance()
{
    return new Robot09();
}


