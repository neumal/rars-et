/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <cmath>

//--------------------------------------------------------------------------
//                           Class Robot07
//--------------------------------------------------------------------------

class Robot08 : public Driver
{

public:
    // Konstruktor
    Robot08()
    {
        // Der Name des Robots
        m_sName = "Robot08";
        // Namen der Autoren
        m_sAuthor = "Kevin Tzschich";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oGREEN;
        m_iTailColor = oYELLOW;
        m_sBitmapName2D = "car_green_green";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
        unsigned long amount_dmg;
        double amount_fuel;
        con_vec result = CON_VEC_EMPTY;
        result.vc = get_speed(s);
        result.alpha = get_direction(s);
        return result;
    }
private:
    // DEFINES
    static const double STEER_GAIN = 0.5;  // gaining of steering
    static const double STEER_DAMP = 1.1;  // damping of steering
    static const double MIN_DIST = 10.2;  // minimum distance to trackwall
    static const double MAX_VC = 200;      // getting maximum speed
    static const double FEET_MILES = 5280; // factor for ft/mile

    double get_speed(situation& s)
    {
        if (s.cur_rad == 0 && s.to_end > 0.4 * s.cur_len)
            return (MAX_VC);
        else if (s.cur_rad == 0 && s.to_end <= 0.4 * s.cur_len)
            return (corn_speed(s.nex_rad));
        else
            return (corn_speed(s.cur_rad));
    }
    double get_direction(situation& s)
    {
        double direction;
        double track_width = s.to_lft+s.to_rgt;
        double offset = 0.0;
        if (s.cur_rad == 0)
        {
            if (s.nex_rad > 0)
                direction = track_width-MIN_DIST;
            else if (s.nex_rad < 0)
                direction = MIN_DIST;
            else
                direction = s.to_rgt;
        }
        else if (s.cur_rad > 0)
        {
            direction = MIN_DIST;
        }
        else if (s.cur_rad < 0)
        {
            direction = track_width - MIN_DIST;
        }
        return (STEER_GAIN * (s.to_lft - direction) / track_width - STEER_DAMP * (s.vn / s.v) + offset);
    }
    // calculates the maximum curve speed in fps
    // radius (in) the curve radius
    // return: maximum curvespeed; -1 on error
    double corn_speed(const double radius)
    {
        if (radius == 0)
            return (-1);
        else
            return (sqrt(fabs(radius * g)));
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot08Instance()
{
    return new Robot08();
}
