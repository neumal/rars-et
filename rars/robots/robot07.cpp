

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot07
//--------------------------------------------------------------------------

class Robot07 : public Driver
{
public:
    // Konstruktor
    Robot07()
    {
        // Der Name des Robots
        m_sName = "Robot07";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oGREEN;
        m_iTailColor = oBROWN;
        m_sBitmapName2D = "car_gray_green";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

     //Diese Funktion hat die Aufgabe den benötigten Bremsweg zu berechnen.
    //Input: Momentane Geschwindigkeit v0, Maximale Kurvengeschwindigkeit v1
    //Output: Bremsweg (critical Distance)
     double CritDist(double v0, double v1)
  {
    double dv;
    double a = -33;     // Feste Bremskraft, durch negative Beschleunigung

    dv = v1 - v0;
    if(dv > 0.0)        // Es ist nicht nötig abzubremsen, wenn die momentane Geschwindigkeit < als die Kurvengeschwindigkeit ist
      return(0.0);
    return (v0 + .5 * dv) * dv / a;
  }

    /* Diese Funktion berechnet die maximale Kurvengeschwindigkeit
       Input: Kurvenradius "radius"
       Output: Kurvengeschwindigkeit in feed/sec */
    double corn_spd(double radius)
    {
        return sqrt(fabs(radius * 32.2*0.95));
    }

    /* Diese Funktion übernimmt die Lenkung des Wagens
       Input: Die Situation S
       Output: Der Lenkwinkel in rad */
    double Lenkung(situation& s)
    {
        double alpha;
        double width = s.to_lft + s.to_rgt;

        /* Das Auto fährt in einer Kurve und kann durch die Vektoren s.vn und s.v den zu fahrenden Winkel berechnen.
        Durch den hinteren Faktor wird der Winkel korrigiert */
        if(s.cur_rad != 0.0)
           {
            return alpha = 5*asin(-(s.vn/s.v)*(s.to_lft+CARWID)/width);
           }
        // Falls das Auto auf einer Geraden fährt, soll das Auto in der Mitte der Strecke fahren.
        else
           {
            return alpha = 0.2*(s.to_lft - s.to_rgt)/width-(s.vn/s.v);
           }
    }

    /* Diese Funktion bestimmt die zufahrenden Geschwindigkeiten
       Input: Situation s
       Output: Die Geschwindigkeit in feet/sec */
    double Geschwindigkeit(situation& s)
    {
        double vc;// Geschwindigkeit
        double width = s.to_lft + s.to_rgt;// Streckenbreite

        /* Auto fährt auf einer Gerade und steuert in ein Kurve.
         Die Geschwindigkeit wird auf die maximale Kurvengeschwindigkeit angepasst.*/
        if(s.cur_rad == 0 && s.to_end < CritDist(s.v,corn_spd(CARWID + s.nex_rad+(s.to_rgt+s.to_lft)/2)))
            return vc = corn_spd(CARWID + s.nex_rad+ (s.to_rgt+s.to_lft)/2);

        /* Auto fährt in einer Kurve und ist noch nicht im Mittelpunkt angekommen.
         Die Geschwindigkeit wird auf die maximale Kurvengeschwindigkeit angepasst.*/
        if(s.cur_rad != 0 && s.cur_len/2 < s.to_end)
            return vc = corn_spd(CARWID + s.cur_rad + (width)/2);

        /* Auto fährt in einer Kurve und hat die Mitte der Kurve erreicht.
         Die Geschwindigkeit wird um den Faktor 2.5 erhöht.*/
        if(s.cur_rad != 0 && s.cur_len/2 > s.to_end)
            return vc = corn_spd(CARWID + s.cur_rad + (width)/2)*1.2;

        // Auto fährt auf einer Gerade und gibt Vollgas.
        if(s.cur_rad == 0)
            return vc = 100;


    }

    con_vec drive(situation& s)
    {
    con_vec result;
    double width = s.to_rgt+s.to_lft;

    result.alpha = Lenkung(s);
    result.vc = Geschwindigkeit(s);

    /* Sonderfall: Der Radius der folgenden Kurve ist kleiner als der Radius der befahrenen Kurve.
       Hier wird die Kurvengeschwindigkeit zu einem früheren Zeitpunkt angepasst. */
    if(s.cur_rad != 0 && s.cur_rad > s.nex_rad && s.to_end < s.cur_len/2)
        result.vc = corn_spd(CARWID + s.nex_rad + (width)/2);

    // Falls das Auto stecken bleibt gelangt es durch die Stuckfunktion auf die Strecke zurück.
    if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))
      return result;

    // Tankfüllung zu Beginn des Rennens
    if( s.starting )
    {
      result.fuel_amount = MAX_FUEL;
    }
    return result;
  }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot07Instance()
{
    return new Robot07();
}
